CREATE DATABASE college;
USE `college`;

SET NAMES utf8 ;

DROP TABLE IF EXISTS `teams`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `teams` (
  `teamId` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`teamId`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

DROP TABLE IF EXISTS `topics`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `topics` (
  `topicId` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  PRIMARY KEY (`topicId`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `classrooms`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `classrooms` (
  `classroomId` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `address` varchar(255) NOT NULL,
  PRIMARY KEY (`classroomId`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `persons`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `persons` (
  `personId` int(11) NOT NULL AUTO_INCREMENT,
  `firstName` varchar(45) NOT NULL,
  `lastName` varchar(45) NOT NULL,
  `email` varchar(45) NOT NULL,
  `dateOfBirth` date NOT NULL,
  `employeeRole` ENUM('trainer','assistant','hr','technician','trainee') NOT NULL DEFAULT '0',
  `teamId` int(11) DEFAULT NULL,
  PRIMARY KEY (`personId`),
  KEY `fk_student_team_idx` (`teamId`),
  CONSTRAINT `fk_student_team` FOREIGN KEY (`teamId`) REFERENCES `teams` (`teamId`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*
  SELECT * FROM persons WHERE employeeRole = 'trainer';
  VS
  SELECT * FROM persons, trainers WHERE trainers.personId IS NOT NULL
*/

-- Justification to hane an ENUM field instead of separate tables:


--  DROP TABLE IF EXISTS `trainers`;
--  /*!40101 SET @saved_cs_client     = @@character_set_client */;
--   SET character_set_client = utf8mb4 ;
--  CREATE TABLE `trainers` (
--    `trainerId` int(11) NOT NULL AUTO_INCREMENT,
--    `personId` int(11) NOT NULL,
--    PRIMARY KEY (`trainerId`),
--    KEY `fk_trainer_person_idx` (`personId`),
--    CONSTRAINT `fk_trainer_person` FOREIGN KEY (`personId`) REFERENCES `persons` (`personId`) ON DELETE NO ACTION ON UPDATE NO ACTION
--  ) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

--  DROP TABLE IF EXISTS `technicians`;
--  /*!40101 SET @saved_cs_client     = @@character_set_client */;
--   SET character_set_client = utf8mb4 ;
--  CREATE TABLE `trainers` (
--    `trainerId` int(11) NOT NULL AUTO_INCREMENT,
--    `personId` int(11) NOT NULL,
--    PRIMARY KEY (`trainerId`),
--    KEY `fk_trainer_person_idx` (`personId`),
--    CONSTRAINT `fk_trainer_person` FOREIGN KEY (`personId`) REFERENCES `persons` (`personId`) ON DELETE NO ACTION ON UPDATE NO ACTION
--  ) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `modules`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `modules` (
  `moduleId` int(11) NOT NULL AUTO_INCREMENT,
  `startDate` date NOT NULL,
  `endDate` date NOT NULL,
  `teamId` int(11) NOT NULL,
  `topicId` int(11) NOT NULL,
  `trainerId` int(11) NOT NULL,
  `classroomId` int(11) NOT NULL,
  PRIMARY KEY (`moduleId`),
  KEY `fk_module_team_idx` (`teamId`),
  KEY `fk_module_subject_idx` (`topicId`),
  KEY `fk_module_trainer_idx` (`trainerId`),
  KEY `fk_module_classroom_idx` (`classroomId`),
  CONSTRAINT `fk_module_classroom` FOREIGN KEY (`classroomId`) REFERENCES `classrooms` (`classroomId`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_module_team` FOREIGN KEY (`teamId`) REFERENCES `teams` (`teamId`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_module_topic` FOREIGN KEY (`topicId`) REFERENCES `topics` (`topicId`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_module_trainer` FOREIGN KEY (`trainerId`) REFERENCES `trainers` (`trainerId`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;


DROP TABLE IF EXISTS `attendances`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `attendances` (
  `attendanceId` int(11) NOT NULL AUTO_INCREMENT,
  `date` date NOT NULL,
  `isPresent` tinyint(1) NOT NULL,
  `moduleId` int(11) NOT NULL,
  `studentId` int(11) NOT NULL,
  PRIMARY KEY (`attendanceId`),
  KEY `fk_attendance_module_idx` (`moduleId`),
  KEY `fk_attendance_student_idx` (`studentId`),
  CONSTRAINT `fk_attendance_module` FOREIGN KEY (`moduleId`) REFERENCES `modules` (`moduleId`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_attendance_student` FOREIGN KEY (`studentId`) REFERENCES `persons` (`personId`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
