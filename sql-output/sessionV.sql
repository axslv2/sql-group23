-- Insert a new entry into employees table:
-- a. employeeId - 1,
-- b. firstName - John,
-- c. lastName - Johnson,
-- d. dateOfBirth - 1975-01-01,
-- e. phoneNumber - 0-800-800-314,
-- f. email - john@johnson.com,
-- g. salary - 1000.

-- INSERT INTO employees(first_name, last_name, birth_date, phone, email, address, salary) VALUES ('John', 'Johnson', '1975-01-01', '0-800-800-314', 'john@johnson.com', 'Address', 1000.00); // ID = 4
-- UPDATE employees SET birth_date = '1980-01-01' WHERE employee_id = 4;

-- DELETE ALL RECORDS
-- DELETE FROM employees;
-- TRUNCATE TABLE employees;
-- 

-- INSERT INTO employees(first_name, last_name, birth_date, phone, email, address, salary) VALUES ('John', 'Johnson', '1975-01-01', '0-800-800-314', 'john@johnson.com', 'Address 1', 1000.00); 
-- INSERT INTO employees(first_name, last_name, birth_date, phone, email, address, salary) VALUES ('James', 'Jameson', '1985-02-02', '0-800-800-315', 'james@jameson.com', 'Address 2', 1500.00); 

-- Select everything from table employees.
-- SELECT * FROM employees;
-- Select only firstName and lastName from table employees.
-- SELECT first_name, last_name FROM employees;
-- Select all employees with lastName Johnson
-- SELECT * FROM employees WHERE last_name = 'Johnson';
-- Select all employees whose lastName starts with J.
-- SELECT * FROM employees WHERE last_name LIKE 'J%';
--  Select all employees whose lastName contains so.
-- SELECT * FROM employees WHERE last_name LIKE '%so%';
-- Select all employees born after 1980.
--  SELECT * FROM employees WHERE birth_date > '1980-01-01';
-- SELECT * FROM employees WHERE YEAR(birth_date) > 1980;
-- Select all employees born after 1980 and whose firstName is John.
-- SELECT * FROM employees WHERE YEAR(birth_date) > 1980 AND first_name = 'John';
-- Select all employees whose lastName is not Jameson.
-- SELECT * FROM employees WHERE last_name != 'Jameson';

-- 10. Select maximum salary.
-- 11. Select minimum salary.
-- 12. Select average salary.
-- SELECT MAX(salary) FROM employees;
-- SELECT MIN(salary) FROM employees;
-- SELECT AVG(salary) FROM employees;

--  See what happens when you add two more entries in employees, this time without setting the
-- employeeId manually:
-- a. 'Julie', 'Juliette', '1990-01-01', '0-800-900-111', 'julie@juliette.com', 5000
-- b. 'Sofie', 'Sophia', '1987-02-03', '0-800-900-222', 'sofie@sophia.com', 1700

-- INSERT INTO employees(first_name, last_name, birth_date, phone, email, address, salary) VALUES ('John', 'Johnson', '1975-01-01', '0-800-800-314', 'john@johnson.com', 'Address 1', 1000.00); 
-- INSERT INTO employees(first_name, last_name, birth_date, phone, email, address, salary) VALUES ('James', 'Jameson', '1985-02-02', '0-800-800-315', 'james@jameson.com', 'Address 2', 1500.00); 
-- INSERT INTO employees(first_name, last_name, birth_date, phone, email, address, salary) VALUES ('Julie', 'Juliette', '1990-01-01', '0-800-900-111', 'julie@juliette.com', 'Address 3', 5000.00);
-- INSERT INTO employees(first_name, last_name, birth_date, phone, email, address, salary) VALUES ('Sofie', 'Sophia', '1987-02-03', '0-800-900-222', 'sofie@sophia.com', 'Address 4', 1700.00);

-- Add two entries in table departments:
-- a. HR,
-- b. Finance.
-- INSERT INTO departments (`name`) VALUES ('HR');
-- INSERT INTO departments (`name`) VALUES ('Finance');

-- assign John to HR and Julie to Finance.
-- UPDATE employees SET department_id = (SELECT department_id FROM departments WHERE name = 'Finance') WHERE employee_id = (SELECT employee_id FROM employees WHERE first_name = 'Julie') -- this is not going to work for mysql

-- ALTER TABLE orders
-- ADD CONSTRAINT fk_orders_customer
-- FOREIGN KEY (customer_id)
-- REFERENCES customers(id)
-- ON UPDATE CASCADE / RESTRICT
-- ON DELETE SET NULL
-- ;





